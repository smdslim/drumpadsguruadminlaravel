import Vue from 'vue';
import Vuex from 'vuex';
import $ from 'jquery';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        app_id: null,
        preset_idx: null,
        app_list: {},
        step_list: [],
        preset_list: []
    },
    getters: {
        app_id: (state) => state.app_id,
        preset_idx: (state) => state.preset_idx,
        app_list: (state) => state.app_list,
        preset_list: (state) => state.preset_list,
        step_list: (state) => state.step_list,
        tutorial: (state) => state.preset_list.find(preset => state.preset_idx == preset.idx).tutorial,
        preset: state => state.preset_list.find(preset => state.preset_idx == preset.idx)
    },
    mutations: {
        initPresetList(state, payload) {
            state.preset_list = payload;
        },
        initStepList(state, payload) {
            state.step_list = payload;
        },
        initAppList(state, payload) {
            state.app_list = payload;
        },
        setAppId(state, app_id) {
            state.app_id = app_id;
        },
        setBpm(state, bpm) {
            // state..tutorial.bpm = bpm;
        },
        setPresetIdx(state, preset_idx) {
            state.preset_idx = preset_idx;
        },
        updateSequence(state, {step_index, row_index, on}) {
            const preset_index = state.preset_list.findIndex(preset => preset.idx === state.preset_idx);
            const index = this.getters.tutorial.sequence[step_index].indexOf(row_index);
            if( on && index === -1 ) {
                state.preset_list[preset_index].tutorial.sequence[step_index].push(row_index);
            } else if( !on && index !== -1 ) {
                state.preset_list[preset_index].tutorial.sequence[step_index].splice(index, 1);
            }
        },
        setStepsAmount(state, new_steps_amount) {
            const preset_index = state.preset_list.findIndex(preset => preset.idx === state.preset_idx);
            const current_steps_amount = this.getters.tutorial.sequence.length;
            if (new_steps_amount < current_steps_amount) {
                state.preset_list[preset_index].tutorial.sequence.splice(new_steps_amount, current_steps_amount - new_steps_amount);
            } else {
                for (let i = current_steps_amount; i < new_steps_amount; i++) {
                    state.preset_list[preset_index].tutorial.sequence.push([]);
                }
            }
        }
    },
    actions: {
        initAppList({commit}, app_list) {
            commit('initAppList', app_list);
        },
        initPresetList({commit}, preset_list) {
            commit('initPresetList', preset_list);
            commit('setPresetIdx', preset_list[0]['idx']);
        },
        initStepList({commit}, step_list) {
            commit('initStepList', step_list);
        },
        setAppId({commit}, app_id) {
            commit('setAppId', app_id);
        },
        loadPresetList({commit}, app_id) {
            $.get(`/preset/getByApp/${app_id}`, (preset_list) => {
                commit('initPresetList', preset_list);
            }, 'json')
        }
    }

});