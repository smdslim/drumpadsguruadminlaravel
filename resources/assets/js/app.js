/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import {library} from '@fortawesome/fontawesome-svg-core';
import {faEdit, faTrashAlt, faTh, faAdjust, faPlay, faStop, faPause} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';

library.add(faEdit, faTrashAlt, faTh, faAdjust, faPlay, faStop, faPause);
Vue.config.productionTip = false;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('pads-block', require('./components/PadsBlock.vue'));
Vue.component('pad', require('./components/Pad.vue'));
Vue.component('pads-colors-select', require('./components/PadsColorsSelect.vue'));
Vue.component('pads-mode-select', require('./components/PadsModeSelect.vue'));
Vue.component('pads-group-select', require('./components/PadsGroupSelect.vue'));
Vue.component('presets-table', require('./components/PresetsTable.vue'));
Vue.component('players-table', require('./components/PlayersTable.vue'));
Vue.component('preset-name', require('./components/PresetName.vue'));
Vue.component('track-table', require('./components/TrackTable.vue'));
Vue.component('step-select', require('./components/StepSelect.vue'));
Vue.component('tutorial-top-controls', require('./components/TutorialTopControls.vue'));
Vue.component('tutorial-track-controls', require('./components/TutorialTrackControls.vue'));
Vue.component('font-awesome-icon', FontAwesomeIcon);

import {store} from './store/store';

window.App = new Vue({
    el: '#app',
    store
});
