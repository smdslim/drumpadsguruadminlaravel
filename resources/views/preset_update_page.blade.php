@extends('layouts.app')

@section('content')
    <div class="container preset-creation-container">
        @if(session('flash'))
            <div class="alert alert-success" role="alert">
                <div>{{session('flash')}}</div>
            </div>
        @endif
        <form method="POST" id="preset-save-form" class="row" action="/preset/patch/{{$preset->idx}}" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <div class="col">
                <table>
                    <tr>
                        <td><label>Application</label></td>
                        <td>
                            <input type="text" class="form-control" disabled value="{{$preset->app->title}}">
                        </td>
                    </tr>
                    <tr>
                        <td><label for="preset-title">Preset title</label></td>
                        <td><input type="text" class="form-control" name="name" maxlength="50" id="preset-title" required value="{{$preset->name}}"></td>
                    </tr>
                    <tr>
                        <td><label for="preset-name">Preset name</label></td>
                        <td><input type="text" class="form-control" name="id" maxlength="50" id="preset-name" required value="{{$preset->id}}"></td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td><label for="preset-files">Preset files</label></td>--}}
                    {{--<td><input type="file" class="form-control" name="files[]" id="preset-files" multiple required></td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td><label for="preset-description">Preset description</label></td>
                        <td>
                            <textarea name="description" class="form-control" id="preset-description" cols="60" rows="5">{{$preset->description}}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="preset-tags">Preset tags</label></td>
                        <td>
                            <textarea name="tag" class="form-control" id="preset-tags" cols="60" rows="2" required maxlength="50">{{$preset->tag}}</textarea>
                        </td>
                    </tr>
                    {{--<tr>--}}
                    {{--<td><label for="preset-logo">Preset logo</label></td>--}}
                    {{--<td><input type="file" name="presetLogoName" class="form-control" id="preset-logo" accept="image/jpeg,image/png,image/gif"></td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td><label for="preset_price">Preset price</label></td>
                        <td><input type="number" name="cost" class="form-control" id="preset_price" value="{{$preset->cost}}"></td>
                    </tr>
                    <tr>
                        <td><label for="preset-tutorial-url">Preset tutorial URL</label></td>
                        <td>
                            <input type="text" name="tutorial_url" class="form-control" id="preset-tutorial-url"
                                   value="{{$preset->tutorial_url}}" required>
                        </td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-primary">UPDATE</button>
            </div>
            <div class="col">
                <pads-block
                        :colors="{{json_encode($colors)}}"
                        :pads_amount="{{$pads_amount}}"
                        :pads="{{json_encode(@$preset->files)}}"
                        :modes="{{json_encode($modes)}}"
                        :cut_groups="{{json_encode($cut_groups)}}"
                >
                </pads-block>
            </div>
        </form>
        @if(count($errors))
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endsection