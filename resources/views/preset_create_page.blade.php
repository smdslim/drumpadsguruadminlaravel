@extends('layouts.app')

@section('content')
    <div class="container preset-creation-container">
        @if(session('flash'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <div>{{session('flash')}}</div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <form method="POST" id="preset-save-form" class="row" action="/preset/store" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="col">
                <table>
                    <tr>
                        <td><label>Application</label></td>
                        <td>
                            <select name="app_id" class="form-control">
                                @foreach($apps as $app)
                                    <option value="{{$app['id']}}">{{$app['title']}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="preset-title">Preset title</label></td>
                        <td><input type="text" class="form-control" name="name" maxlength="50" id="preset-title" required value="{{old('name')}}" @input="$emit('presetTitleInput', $event)"></td>
                    </tr>
                    <tr>
                        <td><label for="preset-name">Preset name</label></td>
                        <td>
                            <preset-name old="{old: '{{old('id')}}'}"></preset-name>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="preset-files">Preset files</label></td>
                        <td><input type="file" class="form-control" name="files[]" id="preset-files" multiple required></td>
                    </tr>
                    <tr>
                        <td><label for="preset-description">Preset description</label></td>
                        <td>
                            <textarea name="description" class="form-control" id="preset-description" cols="60" rows="5">{{old('description')}}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="preset-tags">Preset tags</label></td>
                        <td>
                            <textarea name="tag" class="form-control" id="preset-tags" cols="60" rows="2" required maxlength="50">{{old('tag')}}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="preset-logo">Preset logo</label></td>
                        <td><input type="file" name="presetLogoName" class="form-control" id="preset-logo" accept="image/jpeg,image/png,image/gif"></td>
                    </tr>
                    <tr>
                        <td><label for="preset_price">Preset price</label></td>
                        <td><input type="number" name="cost" class="form-control" id="preset_price" value="{{old('cost')??100}}"></td>
                    </tr>
                    <tr>
                        <td><label for="preset-tutorial-url">Preset tutorial URL</label></td>
                        <td>
                            <input type="text" name="tutorial_url" class="form-control" id="preset-tutorial-url"
                                   value="{{old('tutorial_url')??'#'}}" required>
                        </td>
                    </tr>
                </table>
                <button type="submit" class="btn btn-primary">CREATE</button>
            </div>
            <div class="col">
                <pads-block
                        :colors="{{json_encode($colors)}}"
                        :pads_amount="{{$pads_amount}}"
                        :pads="{{json_encode(@$pads)}}"
                        :modes="{{json_encode($modes)}}"
                        :cut_groups="{{json_encode($cut_groups)}}"
                >
                </pads-block>
            </div>
        </form>
        @if(count($errors))
            <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endsection