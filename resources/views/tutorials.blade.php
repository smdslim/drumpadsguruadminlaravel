@extends('layouts.app')

@section('header')
    <script src="{{ asset('vendor/howler/howler.core.min.js') }}"></script>
@endsection

@section('content')
    <div class="container tutorials-container">
        <tutorial-top-controls ref="tutorial_top_controls"></tutorial-top-controls>
        <tutorial-track-controls></tutorial-track-controls>
        <track-table :initial_steps_amount="{{count($tutorial->sequence)}}"></track-table>
    </div>
@endsection
<script>
    window.TUTORIALS = {
        apps: {!! $apps->toJson() !!},
        steps: {!! $steps->toJson() !!},
        initial_preset_list: {!! $initial_preset_list->toJson() !!},
        app_id: {!! $tutorial->app_id !!}
    }
</script>