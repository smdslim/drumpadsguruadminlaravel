@extends('layouts.app')
@section('content')
    <div class="container preset-list-container">
        <presets-table :presets="{{$presets}}"></presets-table>
    </div>
@endsection