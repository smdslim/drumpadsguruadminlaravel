@extends('layouts.app')
@section('content')
    <div class="container player-list-container">
        <players-table :players="{{$players}}"></players-table>
    </div>
@endsection