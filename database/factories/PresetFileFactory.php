<?php

use Faker\Generator as Faker;

$factory->define(App\PresetFile::class, function (Faker $faker) {
    return [
        'color' => 'blue',
        'mode' => 'tap',
        'cut_group' => 0,
        'duration' => 0
    ];
});
