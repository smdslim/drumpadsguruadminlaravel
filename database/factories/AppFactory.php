<?php

use Faker\Generator as Faker;

$factory->define(App\App::class, function (Faker $faker) {
    return [
        'name' => $faker->word . '_' . $faker->word,
        'title' => $faker->company,
    ];
});