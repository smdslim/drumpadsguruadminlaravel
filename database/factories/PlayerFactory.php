<?php

use Faker\Generator as Faker;

$factory->define(App\Player::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'hash' => $faker->sha1,
        'coin_amount' => rand(0, 1000)
    ];
});
