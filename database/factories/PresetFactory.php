<?php

use Faker\Generator as Faker;

$factory->define(App\Preset::class, function (Faker $faker) {
    return [
        'app_id' => function () {
            return factory('App\App')->create()->id;
        },
        'row_id' => $faker->numberBetween(0, 1000),
        'id' => $faker->word . '_' . $faker->word . '_' . $faker->word,
        'name' => $faker->company,
        'isFree' => true,
        'description' => $faker->sentence,
        'tag' => $faker->sentence,
        'listenPreviewURL' => $faker->url,
        'isDownloadable' => true,
        'presetLogoName' => $faker->word . '.png',
        'downloadURL' => $faker->url,
        'cost' => 100,
        'release_version' => 1,
        'hasTutorial' => true,
        'tutorial_url' => $faker->url
    ];
});
