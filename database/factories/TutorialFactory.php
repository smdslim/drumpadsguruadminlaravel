<?php

use Faker\Generator as Faker;

$factory->define(App\Tutorial::class, function (Faker $faker) {
    return [
        'app_id' => 1,
        'preset_idx' => function () {
            return factory('App\Preset')->create()->idx;
        },
        'sequence' => '[[0,3,9],[],[],[0],[10],[],[0],[],[1,11],[],[],[1],[4],[],[1],[],[2],[],[],[2],[9],[],[2],[],[2,11],[],[],[2],[],[],[2],[],[2,5,9],[],[],[2],[10],[],[2],[],[1,11],[],[],[1],[6],[],[1],[],[0],[],[],[0],[9],[],[0],[],[11],[],[],[],[0],[],[],[]]',
        'bpm' => rand(75, 160),
        'disabled' => '[]',
        'description' => $faker->sentence,
        'title' => $faker->company,
        'type' => 'PRESET'
    ];
});
