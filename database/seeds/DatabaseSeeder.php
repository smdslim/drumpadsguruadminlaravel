<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $user = factory('App\User')->create(['name' => 'smdslim', 'email' => 'test@test.ru']);
        $user->password = bcrypt('asshole');
        $user->save();

        $app_index = 0;
        factory('App\App', 3)->create()->each(function ($app) use ($app_index) {

            // Single valid preset 'No play'
            if ($app_index == 0) {
                $preset = factory('App\Preset')
                    ->create(['app_id' => $app->id, 'id' => 'no_play', 'name' => 'No play']);
                $files = factory('App\PresetFile', 12)->make(['preset_id' => $preset->idx]);
                $files->each(function ($file, $index) use ($preset) {
                    $i = $index + 1;
                    $file->filename = "{$preset->id}{$i}.wav";
                    $file->save();
                });
                factory('App\Tutorial')->create([
                    'app_id' => $app->id,
                    'preset_idx' => $preset->idx
                ]);
            }

            factory('App\Preset', 4)->create(['app_id' => $app->id])->each(function ($preset) use ($app) {
                $files = factory('App\PresetFile', 12)->make(['preset_id' => $preset->idx]);
                $files->each(function ($file, $index) use ($preset) {
                    $i = $index + 1;
                    $file->filename = "{$preset->id}_{$i}.wav";
                    $file->save();
                });
                factory('App\Tutorial')->create([
                    'app_id' => $app->id,
                    'preset_idx' => $preset->idx
                ]);
            });

            $app_index++;
        });

        factory('App\Player', 50)->create();

    }
}
