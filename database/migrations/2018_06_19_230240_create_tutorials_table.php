<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutorials', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('app_id')->default(1);
            $table->unsignedInteger('preset_idx')->unique();
            $table->text('sequence');
            $table->smallInteger('bpm')->default(100);
            $table->text('disabled');
            $table->text('description')->nullable();
            $table->string('title')->nullable();
            $table->enum('type', ['PRESET', 'BASIC'])->default('PRESET');

            $table->foreign('app_id')
                ->references('id')
                ->on('apps')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('preset_idx')
                ->references('idx')
                ->on('presets')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutorials');
    }
}
