<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresetFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preset_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename', 100);
            $table->string('color', 20);
            $table->enum('mode', ['tap', 'hold'])->default('tap');
            $table->unsignedInteger('preset_id');
            $table->tinyInteger('cut_group')->default(-1);
            $table->decimal('duration', 10, 2)->default(0);

            $table->foreign('preset_id')
                ->references('idx')
                ->on('presets')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preset_files');
    }
}
