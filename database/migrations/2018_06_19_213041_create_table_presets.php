<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePresets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presets', function (Blueprint $table) {
            $table->increments('idx');
            $table->unsignedInteger('app_id');
            $table->integer('row_id');
            $table->string('id', 50);
            $table->string('name');
            $table->boolean('isFree')->default(false);
            $table->string('description')->nullable();
            $table->string('tag')->nullable();
            $table->string('listenPreviewURL')->nullable();
            $table->boolean('isDownloadable')->default(false);
            $table->string('presetLogoName', 50)->nullable();
            $table->string('downloadURL')->nullable();
            $table->integer('cost')->default(0);
            $table->integer('release_version')->default(0);
            $table->boolean('hasTutorial')->default(0);
            $table->string('tutorial_url')->nullable();
            $table->tinyInteger('video_is_uploaded')->nullable();
            $table->timestamps();

            $table->unique(['app_id', 'row_id']);
            $table->unique(['app_id', 'id']);

            $table->foreign('app_id')
                ->references('id')
                ->on('apps')
                ->onDelete('restrict')
                ->onUpdate('no action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presets');
    }
}
