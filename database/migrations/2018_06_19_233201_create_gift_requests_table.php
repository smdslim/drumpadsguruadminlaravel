<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gift_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('player_id');
            $table->date('date')->nullable();
            $table->unsignedInteger('gift_id')->default(1);
            $table->unsignedInteger('requests_amount')->default(0);

            $table->unique(['player_id', 'date', 'gift_id']);

            $table->foreign('gift_id')
                ->references('id')
                ->on('gifts')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('player_id')
                ->references('id')
                ->on('players')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gift_requests');
    }
}
