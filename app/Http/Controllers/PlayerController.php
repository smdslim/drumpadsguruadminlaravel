<?php

namespace App\Http\Controllers;

use App\Player;

class PlayerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $players = Player::all();
        return view('player_list', compact('players'));
    }

    public function addCoins(Player $player)
    {
        $player->update(['coin_amount' => $player->coin_amount + request('coin_amount')]);
    }
}

