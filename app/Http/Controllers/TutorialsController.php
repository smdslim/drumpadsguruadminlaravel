<?php

namespace App\Http\Controllers;

use App\App;
use App\Preset;
use App\Tutorial;

class TutorialsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Tutorial $tutorial)
    {
        if (request()->wantsJson()) {
            return $tutorial;
        }
        $apps = App::all();
        $initial_preset_list = Preset::where('app_id', $tutorial->app_id)
            ->with('tutorial:preset_idx,id,sequence,bpm')
            ->get(['app_id', 'idx', 'name', 'id']);
        $steps = collect(['min' => 32, 'max' => 512]);
        return view('tutorials', compact('initial_preset_list', 'steps', 'apps', 'tutorial'));
    }

    public function showTutorials()
    {
        $preset = Preset::orderBy('idx', 'asc')->with('tutorial')->take(1)->first();
        return redirect()->route('tutorial.show', ['id' => $preset->tutorial->id]);
    }

    public function update(Tutorial $tutorial)
    {
        $tutorial->update([
            'sequence' => request('sequence'),
            'bpm' => request('bpm')
        ]);
        if (request()->expectsJson()) {
            return response(['success' => true], 201);
        }
        return back();
    }
}
