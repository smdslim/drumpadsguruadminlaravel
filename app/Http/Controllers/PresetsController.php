<?php

namespace App\Http\Controllers;

use App\App;
use App\Preset;
use App\PresetFile;
use Illuminate\Http\Request;

class PresetsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createPage()
    {
        $apps = App::all();
        extract($this->prepareDataForPresetPage());
        return view('preset_create_page', compact('pads_amount', 'colors', 'group_colors', 'modes', 'cut_groups', 'apps'));
    }

    public function updatePage(Preset $preset)
    {
        extract($this->prepareDataForPresetPage());
        return view('preset_update_page', compact('preset', 'pads_amount', 'colors', 'group_colors', 'modes', 'cut_groups'));
    }

    private function prepareDataForPresetPage()
    {
        return [
            'pads_amount' => 12,
            'colors' => ['orange', 'pink', 'blue', 'green'],
            'group_colors' => ['#f1ffa7', 'antiquewhite', 'aqua', 'aquamarine'],
            'modes' => ['tap', 'hold'],
            'cut_groups' => range(-1, 3)];
    }

    public function store()
    {
        request()->validate([
            'app_id' => 'required',
            'id' => 'required',
            'name' => 'required',
            'tag' => 'required',
            'files' => 'required|array|size:12|only_wav',
            'presetLogoName' => 'required|image',
            'tutorial_url' => 'required'
        ]);

        // Create preset
        $preset = $this->createPresetFromRequest();

        // Store audio files on disk and also in db
        $this->storeAudioFiles($preset);

        // Store preset logo image
        $this->storePresetLogo($preset, request()->file('presetLogoName'));

        if (request()->wantsJson()) {
            return response($preset, 201);
        }
        return back()->with('flash', 'Preset successfully created');
    }

    public function patch(Preset $preset)
    {
        // Validate
        $preset_data = request()->validate([
            'id' => 'required',
            'name' => 'required',
            'tag' => 'required',
            'tutorial_url' => 'required',
            'cost' => 'required'
        ]);
        $pads_data = request()->validate([
            'colors' => 'required',
            'modes' => 'required',
            'cut_groups' => 'required'
        ]);
        // Update preset
        $preset->update($preset_data);
        // Delete old pads data
        $preset->files()->delete();
        // Insert new pads data
        for ($i = 1; $i <= 12; $i++) {
            $data = [];
            $data['filename'] = "{$preset->id}{$i}.wav";
            $data['preset_id'] = $preset->idx;
            $data['color'] = $pads_data['colors'][$i];
            $data['mode'] = $pads_data['modes'][$i];
            $data['cut_group'] = $pads_data['cut_groups'][$i];
            $preset->files()->create($data);
        }
        return back()->with('flash', 'Preset updated');
    }

    protected function storeAudioFiles($preset)
    {
        collect(request()->file('files'))->each(function ($file, $index) use ($preset) {
            $file_name = $file->getClientOriginalName();
            $file->storeAs("sounds/{$preset->id}", $file_name, 'public');
            $preset->files()->create([
                'filename' => $file_name,
                'color' => request('colors')[$index + 1],
                'mode' => request('modes')[$index + 1],
                'preset_id' => $preset->idx,
                'cut_group' => request('cut_groups')[$index + 1],
                'duration' => 0 //TODO: create duration calculate method
            ]);
        });
    }

    protected function storePresetLogo($preset, $file)
    {
        $image_file_name = $file->getClientOriginalName();
        $file->storeAs("sounds/{$preset->id}", $image_file_name, 'public');
    }

    protected function createPresetFromRequest()
    {
        return Preset::create([
            'app_id' => request('app_id'),
            'id' => request('id'),
            'name' => request('name'),
            'description' => request('description'),
            'tag' => request('tag'),
            'presetLogoName' => request()->file('presetLogoName')->getClientOriginalName(),
            'cost' => request('cost'),
            'tutorial_url' => request('tutorial_url')
        ]);
    }

    public function list()
    {
        $presets = Preset::all();
        return view('preset_list', compact('presets'));
    }

    public function destroy(Preset $preset)
    {
        Preset::destroy($preset->idx);
    }

    public function getByApp($app_id)
    {
        $presets = Preset::where('app_id', $app_id)
            ->with('tutorial:preset_idx,id,sequence,bpm')
            ->get(['app_id', 'idx', 'name', 'id']);
        return $presets->toJson();
    }

}
