<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preset extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'idx';
    protected $routeKeyName = 'idx';

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($preset) {

            $row_id = 1;
            $release_version = 1;

            // Select last row id for the current app
            $data = static::whereAppId($preset->app_id)->orderBy('idx', 'desc')->first();
            if ($data) {
                $row_id = ++$data->row_id;
                $release_version = ++$data->release_version;
            }
            $preset->attributes['row_id'] = $row_id;
            $preset->attributes['release_version'] = $release_version;

        });
    }

    public function getRouteKeyName()
    {
        return $this->routeKeyName;
    }

    public function files()
    {
        return $this->hasMany(PresetFile::class, 'preset_id', 'idx');
    }

    public function tutorial()
    {
        return $this->hasOne(Tutorial::class, 'preset_idx', 'idx');
    }

    public function app()
    {
        return $this->belongsTo(App::class);
    }


}
