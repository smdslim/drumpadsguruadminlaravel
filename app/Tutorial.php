<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function getSequenceAttribute($value)
    {
        return json_decode($value);
    }

    public function preset()
    {
        return $this->belongsTo(Preset::class);
    }
    
}
