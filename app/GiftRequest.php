<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiftRequest extends Model
{
    public $timestamps = false;
}
