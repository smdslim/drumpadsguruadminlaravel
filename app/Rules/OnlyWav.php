<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OnlyWav implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $valid_mimes = ['audio/x-wav', 'audio/wav'];
        if (is_array($value)) {
            foreach ($value as $file) {
                if (!in_array($file->getClientMimeType(), $valid_mimes)) return false;
            }
            return true;
        }
        return in_array($value->getClientMimeType(), $valid_mimes);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
