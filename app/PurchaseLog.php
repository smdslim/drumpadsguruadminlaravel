<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseLog extends Model
{
    public $timestamps = false;
}
