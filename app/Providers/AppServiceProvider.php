<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        \Validator::extend('only_wav', 'App\Rules\OnlyWav@passes');

        \Blade::directive('is_active_route', function ($name) {
            return "<?php if(Route::currentRouteName() == '$name') echo 'active'; ?>";
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (app()->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
