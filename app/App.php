<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    public $timestamps = false;
    protected $guarded = [];


    function presets(){
        return $this->hasMany(Preset::class);
    }

}
