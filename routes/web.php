<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/preset/create', 'PresetsController@createPage')->name('preset.create.show');
Route::get('/preset/update/{preset}', 'PresetsController@updatePage')->name('preset.update.show');
Route::get('/preset/list', 'PresetsController@list')->name('preset.list');
Route::post('/preset/store', 'PresetsController@store')->name('preset.store');
Route::patch('/preset/patch/{preset}', 'PresetsController@patch')->name('preset.patch');
Route::delete('/preset/delete/{preset}', 'PresetsController@destroy')->name('preset.delete');
Route::get('/preset/getByApp/{app_id}', 'PresetsController@getByApp')->name('preset.get_by_app');

Route::get('/players', 'PlayerController@show')->name('players.show');
Route::patch('/players/{player}/add_coins', 'PlayerController@addCoins')->name('players.add_coins');

Route::get('/tutorial/{tutorial}', 'TutorialsController@show')->name('tutorial.show');
Route::get('/tutorials', 'TutorialsController@showTutorials')->name('tutorials.show');
Route::patch('/tutorial/{tutorial}', 'TutorialsController@update')->name('tutorial.patch');
