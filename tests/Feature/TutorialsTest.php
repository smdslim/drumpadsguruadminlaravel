<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TutorialsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function a_logged_in_user_can_view_tutorials_page()
    {
        $tutorial = factory('App\Tutorial')->create();
        $this->signIn();
        $this->get(route('tutorial.show', ['id' => $tutorial->id]))
            ->assertStatus(200)
            ->assertSee('track-table');
    }

    /** @test */
    public function only_logged_in_user_can_view_tutorials()
    {
        factory('App\Tutorial')->create();
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->get('/tutorial/1');
    }

    /** @test */
    public function preset_can_be_updated()
    {
        $this->signIn();
        $preset = factory('App\Preset')->create();
        $tutorial = factory('App\Tutorial')->create(['app_id' => $preset->app_id, 'preset_idx' => $preset->idx])->toArray();
        $tutorial['sequence'] = json_encode(['a' => 'b']);
        $this->patch(route('tutorial.patch', ['id' => $tutorial['id']]), $tutorial);
        $this->assertDatabaseHas('tutorials', ['sequence' => "{\"a\":\"b\"}"]);
    }

}
