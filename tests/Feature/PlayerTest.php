<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PlayerTest extends TestCase
{
    use DatabaseMigrations;


    /** @test */
    public function it_checks_if_coins_can_be_added()
    {
        $this->signIn();
        // Given we have a player
        $player = factory('App\Player')->create(['coin_amount' => 0]);
        // Add coins via patch request
        $this->patch('/players/1/add_coins', ['coin_amount' => 20]);
        // Check if coins updated in db
        $player = $player->fresh();
        $this->assertEquals(20, $player->coin_amount);
    }

    /** @test */
    public function it_checks_if_unsigned_user_cannot_add_coins()
    {
        factory('App\Player')->create(['coin_amount' => 0]);
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->patch('/players/1/add_coins', ['coin_amount' => 20]);
    }
}
