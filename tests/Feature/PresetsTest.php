<?php

namespace Tests\Feature;

use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PresetsTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function it_checks_if_db_restricts_same_preset_names_for_the_same_app()
    {
        // Given we have a preset for app id = 1
        factory('App\App')->create();
        $preset = factory('App\Preset')->create(['app_id' => 1]);
        // Create a new preset with the same name
        $this->expectException(QueryException::class);
        // It should prevent saving preset to database
        factory('App\Preset')->create(['app_id' => 1, 'id' => $preset->id]);
    }

    /** @test */
    public function it_checks_if_preset_saves_to_db_successfully()
    {
        $this->signIn();
        $this->withExceptionHandling();

        $preset = factory('App\Preset')->make();

        $preset = $preset->toArray();
        $preset['files'] = [];
        Storage::fake('public');
        // create audio files
        for ($i = 1; $i <= 12; $i++) {
            $preset['files'][] = UploadedFile::fake()->create($preset['id'] . "_{$i}.wav", 30);
        }
        // create preset logo file
        $preset['presetLogoName'] = UploadedFile::fake()->create($preset['id'] . '_logo.png', 10);
        $first_file = $preset['files'][0]->getClientOriginalName();

        $preset['colors'] = $this->generatePadsArray(['orange', 'blue', 'pink', 'green']);
        $preset['cut_groups'] = $this->generatePadsArray([-1, 0, 2, 3]);
        $preset['modes'] = $this->generatePadsArray(['tap', 'hold']);

        $response = $this->json('post', '/preset/store', $preset);
        if ($response->status() !== 201) {
            $this->fail(json_encode($response->getData()));
        }
        $response->assertStatus(201);
        $this->assertDatabaseHas('preset_files', ['filename' => $preset['id'] . "_1.wav"]);
        Storage::disk('public')->assertExists("sounds/{$preset['id']}/" . $first_file);
        Storage::disk('public')->assertExists("sounds/{$preset['id']}/" . $preset['presetLogoName']->getClientOriginalName());
    }

    /** @test */
    public function it_checks_for_valid_row_id_and_release_version_id()
    {
        factory('App\App')->create(['id' => 1]);
        factory('App\App')->create(['id' => 2]);

        $this->createPreset(['app_id' => 1, 'row_id' => 1]);
        $this->createPreset(['app_id' => 2, 'row_id' => 1]);
        $this->createPreset(['app_id' => 2, 'row_id' => 2]);
        $preset = $this->createPreset(['app_id' => 2, 'row_id' => 2]);

        $this->assertEquals(3, $preset->row_id);
        $this->assertEquals(3, $preset->release_version);
    }

    /** @test */
    public function it_can_view_preset_creation_page()
    {
        $this->signIn();
        $app = factory('App\App')->create();
        $this->get('/preset/create')
            ->assertSee($app->title);
    }

    /** @test */
    public function it_can_remove_preset()
    {
        $this->signIn();
        $preset = $this->createPreset();
        $this->json('DELETE', "/preset/delete/{$preset->idx}")
            ->assertStatus(200);
        $this->assertDatabaseMissing('presets', ['idx' => $preset->idx]);
    }

    /** @test */
    public function it_checks_unsigned_user_cannot_remove_preset()
    {
        $preset = $this->createPreset();
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->json('DELETE', route('preset.delete', ['idx' => $preset->idx]));
    }

    /** @test */
    public function it_checks_unsigned_user_cannot_create_preset()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->json('POST', route('preset.store', []));
    }

    /** @test */
    public function it_checks_unsigned_user_cannot_update_preset()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->json('PATCH', route('preset.patch', ['idx' => 1]));
    }

    protected function generatePadsArray($possible_values)
    {
        $result = [];
        for ($i = 1; $i <= 12; $i++) {
            $result[$i] = $possible_values[rand(0, count($possible_values) - 1)];
        }
        return $result;
    }

    protected function createPreset($attributes = [])
    {
        $preset = factory('App\Preset')->create($attributes);
        // create audio files
        for ($i = 0; $i < 12; $i++) {
            $preset->files = factory('App\PresetFile', 12)->create(['preset_id' => $preset->idx, 'filename' => $preset->id . '.wav']);
        }
        return $preset;
    }

}
