<?php

namespace Tests\Browser;

use App\User;
use Faker\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class ExampleTest extends DuskTestCase
{
    public function testBasicExample()
    {
        $faker = Factory::create('en_GB');
        $this->browse(function (Browser $browser) use ($faker) {
            $browser->loginAs(User::find(1));
            $browser->visit('/preset/create')
                ->type('name', $faker->name)
                ->type('id', $faker->word . '_' . $faker->word . '_' . $faker->word)
                ->type('description', $faker->sentence)
                ->attach('files[]', __DIR__ . '/resources/no_play_1.wav')
                ->type('tag', '#' . $faker->word);
            $browser->click('button[type="submit"]');
            $browser->assertSee('Create preset');
            $browser->stop();
        });
    }

}
